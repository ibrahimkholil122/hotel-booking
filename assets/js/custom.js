$(".category-item").mouseenter(function () {
    $(this).toggleClass('cat-active');
    var data_id = $(this).data('id');

    $('.catagory-gallery-item').each(function() {
        var el = $(this);

        if(el.attr('id') == data_id)
            el.show('1000');

        else
            el.hide('1000');

    });
});

// Price range slider

$( function() {
    $( "#filter-price" ).slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 0, 100 ],
        slide: function (event, ui) {
            //$("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            $('.ui-slider-handle:first').html('<div class="filter-price-left"><div class="filter-price-inner">'  + "$" + ui.values[0] + '</div></div>');
            $('.ui-slider-handle:last').html('<div class=" filter-price-right"><div class="filter-price-inner">' + "$" + ui.values[1] + '</div></div>');
        }
    });
} );

// slick slider for grid list
$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: false,
    focusOnSelect: true
});


// Single List Slider
$('#DetailsListSlider').slick({

    variableWidth: true,
    slidesToScroll: 3,
    slidesToShow: 3,
    speed: 300,
    prevArrow: '<div class="slick-prev"><span class="prev-icon"></span></div>',
    nextArrow: '<div class="slick-next"><span class="next-icon"></span></div>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
        }
    ]
});
// $('#DetailsListSlider').slick({
//     arrows: true,
//     dots: false,
//     infinite: true,
//     speed: 300,
//     slidesToShow: 1,
//     centerMode: true,
//     variableWidth: true,
//     prevArrow: '<div class="slick-prev"><span class="prev-icon"></span></div>',
//     nextArrow: '<div class="slick-next"><span class="next-icon"></span></div>',
//     responsive: [{
//         breakpoint: 1024,
//         settings: {
//             slidesToShow: 3,
//             slidesToScroll: 3,
//             infinite: true,
//             dots: false
//         }
//     }, {
//         breakpoint: 600,
//         settings: {
//             slidesToShow: 2,
//             slidesToScroll: 2
//         }
//     }, {
//         breakpoint: 480,
//         settings: {
//             slidesToShow: 1,
//             slidesToScroll: 1
//         }
//     }]
// });
// single list Popup slider


$(".singleListPopupSlide").slick({
    dots: false,
    infinite: false,
    speed: 300,
    prevArrow: '<div class="slick-prev"><span class="prev-icon"></span></div>',
    nextArrow: '<div class="slick-next"><span class="next-icon"></span></div>',
});

$(".tabs").on("toggled", function (event, tab) {
    $(".singleListPopupSlide").slick("setPosition");
});
// smiliar itemes slider

$('#similarProductSlide').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    prevArrow: '<div class="slick-prev"><span class="prev-icon"></span></div>',
    nextArrow: '<div class="slick-next"><span class="next-icon"></span></div>',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});


$('#yourRooms').slick({
    dots: true,
    infinite: true,
    speed: 500,
    fade: false,
    slide: 'li',
    cssEase: 'linear',
    centerMode: true,
    slidesToShow: 1,
    variableWidth: true,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    prevArrow: '<div class="slick-prev"><span class="prev-icon"></span></div>',
    nextArrow: '<div class="slick-next"><span class="next-icon"></span></div>',
    responsive: [{
        breakpoint: 800,
        settings: {
            arrows: false,
            centerMode: false,
            centerPadding: '40px',
            variableWidth: false,
            slidesToShow: 1,
            dots: true
        },
        breakpoint: 1200,
        settings: {
            arrows: false,
            centerMode: false,
            centerPadding: '40px',
            variableWidth: false,
            slidesToShow: 1,
            dots: true

        }
    }],
    customPaging: function (slider, i) {
        return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
    }
});


// Tooltips
$('.tip').each(function () {
    $(this).tooltip(
        {
            html: true,
            title: $('#' + $(this).data('tip')).html()
        });
});


